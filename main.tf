# provider block. 


terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "5.31.0"
    }
  }
}

provider "aws" {
  # Configuration options
  region = "us-east-1"
}

# creating our VPC.
resource "aws_vpc" "cp_vpc01" {
  cidr_block       = "10.29.0.0/16"
  instance_tenancy = "default"

  tags = {
    Name = "cpb29-vpc-01"
  }
}


resource "aws_subnet" "subnet_1" {
  vpc_id                  = aws_vpc.cp_vpc01.id
  cidr_block              = "10.29.1.0/24"
  availability_zone       = "us-east-1a"
  map_public_ip_on_launch = true

  tags = {
    Name = "cpb29-pub-sub-1a"
  }
}

resource "aws_subnet" "subnet_2" {
  vpc_id                  = aws_vpc.cp_vpc01.id
  cidr_block              = "10.29.2.0/24"
  availability_zone       = "us-east-1b"
  map_public_ip_on_launch = true

  tags = {
    Name = "cpb29-pub-sub-1b"
  }
}

resource "aws_internet_gateway" "cp_igw" {
  vpc_id = aws_vpc.cp_vpc01.id

  tags = {
    Name = "cpb29-igw"
  }
}


resource "aws_route_table" "cp_pub_route_table" {
  vpc_id = aws_vpc.cp_vpc01.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.cp_igw.id
  }

  tags = {
    Name = "cpb29-pub-route-table"
  }
}

resource "aws_route_table_association" "a" {
  subnet_id      = aws_subnet.subnet_1.id
  route_table_id = aws_route_table.cp_pub_route_table.id
}

resource "aws_route_table_association" "b" {
  subnet_id      = aws_subnet.subnet_2.id
  route_table_id = aws_route_table.cp_pub_route_table.id
}

resource "aws_network_acl" "cp_pub_nacl" {
  vpc_id = aws_vpc.cp_vpc01.id

  egress {
    protocol   = "tcp"
    rule_no    = 100
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 1024
    to_port    = 65535
  }

  egress {
    protocol   = "tcp"
    rule_no    = 200
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 443
    to_port    = 443
  }

  ingress {
    protocol   = "tcp"
    rule_no    = 100
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 22
    to_port    = 22
  }

  ingress {
    protocol   = "tcp"
    rule_no    = 200
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 80
    to_port    = 80
  }

  ingress {
    protocol   = "tcp"
    rule_no    = 300
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 1024
    to_port    = 65535
  }


  tags = {
    Name = "cpb29-pub-nacl"
  }
}


resource "aws_network_acl_association" "cp_nacl_association1" {
  network_acl_id = aws_network_acl.cp_pub_nacl.id
  subnet_id      = aws_subnet.subnet_1.id
}

resource "aws_network_acl_association" "cp_nacl_association2" {
  network_acl_id = aws_network_acl.cp_pub_nacl.id
  subnet_id      = aws_subnet.subnet_2.id
}


data "aws_ami" "amazon_linux_2" {
  most_recent = true

  filter {
    name   = "name"
    values = ["al2023-ami-2023.3.20231218.0-kernel-6.1-x86_64"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["137112412989"] # Canonical
}

resource "aws_security_group" "allow_ssh_http" {
  name        = "allow_ssh"
  description = "Allow SSH inbound traffic"
  vpc_id      = aws_vpc.cp_vpc01.id

  ingress {
    description = "SSH from internet"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "http from internet"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "allow_ssh_http"
  }
}

resource "aws_instance" "web" {
  ami                    = data.aws_ami.amazon_linux_2.id
  instance_type          = "t3.micro"
  key_name               = "cp-b29-us-east-1"
  subnet_id              = aws_subnet.subnet_1.id
  vpc_security_group_ids = [aws_security_group.allow_ssh_http.id]


  tags = {
    Name = "cpb29-linux"
  }
}
